# Twitch::Bot

![](https://github.com/geewiz/twitch-bot/workflows/Ruby%20Gem/badge.svg)

`twitch-bot` provides a Twitch chat client object that can be used for building Twitch chat bots.

This gem is based on [`twitch-chat`](https://github.com/EnotPoloskun/twitch-chat).

## Installation

Add this line to your application's `Gemfile`:

```ruby
gem 'twitch-bot'
```

Install all the dependencies:

```
$ bundle
```

Or install it manually via:

```
$ gem install twitch-bot
```

## Usage

twitch-bot uses the Twitch IRC protocol to connect to Twitch chat. For every chat message sent by a user or by the system, twitch-bot creates an `Event` object that will be passed to all `EventHandler` objects that subscribe to its event type.

### Code example

```ruby
class MyBot
  def initialize
    # Create a new client instance
    @client = Twitch::Bot::Client.new(
      channel: "twitch_user_name",
      config: configuration,
    ) do
      # Register an event handler, see below
      register_handler(JoinHandler)
    end
  end

  def run
    # Launch chat client
    client.run
  end

  private

  attr_reader :client

  def configuration
    Twitch::Bot::Config.new(
      settings: {
        botname: "mybot",
        irc: {
          nickname: "twitch_user_name",
          password: "twitch_user_password",
        },
        adapter: "Twitch::Bot::Adapter::Irc",
      }
    )
  end
end

# Join handler gets called when the bot joins the channel
class JoinHandler < Twitch::Bot::EventHandler
  # This method gets called when the event is triggered
  def call
    client.send_message "Bot reports in ready for work!"
  end

  # Subscribe this handler to join events
  def self.handled_events
    [:join]
  end
end
```

For a real-life bot implementation, please refer to the [Teneggs](https://www.gitlab.com/geewiz/teneggs) repository.

## Contributing

1. Fork the repo (https://gitlab.com/geewiz/twitch-bot/fork)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -a`)
4. Push the branch (`git push origin my-new-feature`)
5. Submit a Merge Request from your repository

Please take note of the Code Of Conduct in `CODE_OF_CONDUCT.md`.
